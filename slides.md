# Sistema de Expedientes
## desarrollo de una aplicación móvil

por Lic. Christian A. Rodriguez

<small>
<car@cespi.unlp.edu.ar>
<br />
http://chrodriguez.gitlab.io/concurso-no-docente/
</small>
***
# Presentación
---
## Temario

* Introducción
* Metodología de desarrollo
* Arquitectura
* Herramientas de desarrollo
* Planificación
* Infraestructura
* Integración ISO9001
***
***
# Introducción
---
## La historia

* A mediados de 2004 se inició el relevamiento del sistema
* En 2006 se finalizó el desarrollo e implementó:
  * Utilizando PHP 5.1 y base de datos Mysql 4.1
  * No empleaba ningún fraemework
* En 2009 se comenzó una reescritura empleando Symfony
  * Se utilizó symfony 1.2
  * Se preservó la base de datos original, actualizándola
* En 2012 se pone en producción la nueva versión
  * La migración fue silenciosa
  * Se agregaron nuevas funcionalidades
  * Servicio SOAP de búsqueda de expedientes
---
## Actualidad del sistema

* Se mantiene estable desde 2013
* Se han incorporado datos de sistemas legados de expedientes de algunas dependencias
* No se ha integrado con las nuevas tecnologías adoptadas por CeSPI:
  * SSO
  * API integrada de servicios
---
## Contextualizando el proyecto

_Para poder diseñar una versión móvil del sistema debe definirse si la versión móvil contará con la misma funcionalidad que la versión online o no. 
En este proyecto **asumiré que se desea proveer la misma funcionalidad en ambas versiones**_

---
## Contextualizando el proyecto
* En este hipotético caso, un rediseño completo será la base de este pre-proyecto:
  * Un desarrollo de igual funcionalidad de la aplicación legada, podría ocasionar inconsistencias.
  * Sería la oportunidad de actualizar e integrar con SSO y API integrada de servicios
  * Analizar los procedimientos que han sido informatizados y que podrían simplificar la gestión de expedientes mediante la integración de los sistemas
---
## Incompatibilidades

El actual sistema limita su acceso a la red de la UNLP. **Este hecho restringiría el uso de la aplicación móvil.**

* En tal caso, se asume la apertura del sistema sin imponer restricciones considerando:
  * Uso de SSL
  * Integración con SSO
  * Pruebas de seguridad a cargo del CERT UNLP

***
***
# Objetivo
---
## Objetivo

El objetivo del nuevo desarrollo es el de maximizar la eficiencia del uso del sistema de expedientes, que hoy día es empleado por todas las dependencias de la UNLP, aportando una interfaz más intuitiva, cómoda y disponible en dispositivos móviles. 
*De esta forma afianzamos nuestra intención de alcanzar la satisfacción de los usuarios de nuestros sistemas que enuncia nuestra política de calidad*

***
***
# Metodología de desarrollo
---
## Introducción

La metodología empleada en la oficina de Desarrollo, es una metodología **ágil híbrida**, donde se promueve el trabajo en equipos que atienden el desarrollo de no más de un proyecto a la vez, pero donde además deben atenderse pedidos de mantenimientos de otros proyectos ya en producción.

Antes de proceder con un **nuevo desarrollo**, se realiza un análisis de los requerimientos globales y se vuelcan en la herramienta de gestión de tickets, dividiéndolos en potenciales versiones.

---
## Relevamiento

* Identificar el **dueño del sistema**
* Conformar un comitée de usuarios del sistema, liderado por un responsable de la toma de decisiones
* Confeccionar una propuesta del desarrollo que básicamente reimplmenta la funcionalidad actual, y propone:
  * Integración con SSO
  * Utilización de la API integrada de servicios
---
## Relevamiento
* Coordinar reuniones con el comitée donde se presente la propuesta y se analicen nuevas funcionalidades que surjan de la modernización de otros sistemas que se relacionan con el actual sistema
* Revisar los circuitos por tipo de expediente, depurándo y mejorándolos
* El resultado de la primer reunión con el comitée arrojará una planificación de reuniones previas al inicio del desarrollo
---
## Análisis de los requerimientos

* En base al relevamiento realizado, se analizan los requerimientos según la arquitectura a adoptar para el nuevo desarrollo
* Los requerimientos se explotan en unidades pequeñas, que sean desarrollables en no más de 3 días
* Los requerimientos se agrupan en versiones según priorización de los requerimientos. Cada versión será funcional y se tratará de obtener una nueva versión cada 30 días.
* Se proponen reuniones con el comitée cada 30 días mostrando los avances en cada nueva versión
***
***
# Arquitectura
---
## Arquitectura

Se propone una arquitectura de servicios constituída por:

* Un backend RESTfull que implemente la lógica completa sin interfaz más que los endpoints de la API
  * Interactuaría con la API integrada de servicios
  * Una base de datos relacional o NoSQL (a definir durante la implementación)
  * Una base de datos NoSQL que permita la indexación de datos y simplifique la implementación de búsquedas
* Dos frontend visuales que iteractúen con el backend
  * Cada frontend utilizará como base de autenticación el framework SSO adoptado
***
***
# Herramientas de desarrollo
---
## Herramientas de gestión

* Para el manejo de tickets y seguimiento de requerimientos: Redmine
* Para el versionado de código: GIT
* Para integración contínua: Gitlab CI
* Para deployment/delivery contínuo: Gitlab CI

---
## Lenguajes y Frameworks

* Dado que cada servicio es muy diferente en cuanto a lo que proveen, se proponen diferentes herramientas de desarrollo para cada uno:
  * **Backend:** desarrollo Ruby on Rails, utilizando Rails API que implementa el estandar JSON API
  * **Frontend:** desarrollado en Javascript, utilizando ReactJS, dado que la versión móvil puede adaptarse con React Native

 [![Ruby on Rails](images/rails-logo.jpg "Ruby on Rails"){: class="logo"}](http://rubyonrails.org/) [![json:api](images/jsonapi-logo.png "json api"){: class="logo"}](http://jsonapi.org/) [![React JS](images/react-logo.jpg "React JS"){: class="logo"}](https://facebook.github.io/react/) [![React Native](images/reactnative-logo.png "React Native"){: class="logo"}](https://facebook.github.io/react-native/)
---
## Bases de datos

* Para el modelo de datos que implemente la lógica de negocio se proponen o:
  * MongoDB
  * Mysql
* Elastic para la indexación de contenidos

<small>
_**La decisión de MongoDB o Mysql se tomará durante la implementación, según pruebas realizadas**_
</small>

[![MySQL](images/mysql-logo.jpg "MySQL"){: class="logo"}](https://www.mysql.com/) [![MongoDB](images/mongodb-logo.jpg "Mongodb"){: class="logo"}](https://www.mongodb.com/) [![Elastic](images/elastic-logo.png "Elastic"){: class="logo"}](https://www.elastic.co/) 

***
***
# Planificación
---
## Recursos

* Se estiman los siguientes recursos para el desarrollo:
  * **Líder de proyecto:** _**una**_ persona con asignación de 2 horas diaras en este proyecto
  * **Analista funcional:** _**una**_ persona con asignación de 4 horas diarias el primer mes, y 2 horas diarias durante la ejecución del proyecto
  * **Programadores backend:** _**dos**_ semi senior con asignación de 6 horas diarias
  * **Programadores frontend:** _**un**_ semi senior y un junior con asignación de 6 horas diarias
---
## Estimación de tiempo

* **Analisis de requerimientos:** en 2 meses debe obtenerse la lista de tickets a implementar agrupados en versiones
* **Diseño de los endpoints de la API del backend:** en 1 mes debe diseñarse los endpoints más importantes y ejemplificarlos con la finalidad de implementarlos en la API integrada de servicios en modo **simulado**
  * _Esto permite a los desarrolladores del frontend y backend trabajar sobre una API mock_
* **Desarrollo del backend:** aproximadamente 4 meses
* **Desarrollo del frontend:** aproximadamente 5 meses
* **Pruebas de aceptación:** aproximadamente 2 meses

<small>La paralelización del desarrollo del backend y frontend
dependerá del diseño previo de la API
<br />
**El desarrollo considera implementación de pruebas**</small>
***
***
# Infraestructura
---
## Servicios involucrados

* Para instalar el stack completo de esta nueva aplicación se requieren los sigueintes servicios
  * Una base de datos MySQL o MongoDB
  * Un cluster Elastic de 3 nodos
  * Un backend API
  * Dos frontends

Alguno de los servicios antes mencionados pueden verse afectados con una alta carga de accesos al frontend, es por ello que se propone permitir el escalado horizontal del backend como los frontends

<small>
El balanceo de los servicios escalados se realizará con **HAProxy**
</small>

---
## Virtualización

* Los equipos que componen la infraestructura de toda la oficina, se encuentran
  virtualizados
* Virtualización basada en VMWare VSPhere
* Las virtuales corren Ubuntu 16.10, con docker engine
* VMWare VSPhere aprovisionado con chef-provisioning
  * Los servidores se crean y destruyen con recetas de chef
  * Los servidores se aprovisionan con chef

**Esto promueve una infreatructura repetible**
 [![VMWare](images/vmware-logo.jpg "VMWare"){: class="logo"}](http://www.vmware.com/latam/products/vsphere.html) [![Chef](images/chef-logo.png "CHEF"){: class="logo"}](https://www.chef.io/) [![Docker Engine](images/docker-logo.png "Docker Engine"){: class="logo"}](https://www.docker.com/)
---
## Docker

* El uso de docker se realiza en forma de cluster, usando Cattle, la solución provista por Rancher [![Rancher](images/rancher-logo.png "Rancher"){: class="logo-min"}](http://rancher.com/)
  * **Servidor rancher:** compuesto por dos servidores rancher en alta disponibilidad, balanceados con un nginx, con un servidor MySQL independiente
  * Ambientes independientes Cattle por tipo de aplicación: cada ambiente tiene al menos dos nodos docker de forma tal de proveer alta disponibilidad
  * Los nodos del cluster utilizan NFS como storage para sus clusters
  * Los servicios se exponen a través de un contenedor HAProxy que además de balancear silenciosamente los upgrades/rollback, rompe SSL

---
## Los beneficios

* Toda la infrestructura está versionada como código
* Rancher permite backupear la infraestructura de todos los ambientes con un simple dump de MySQL
* Sólo debemos realizar backups del storage NFS, y de la base de datos de Rancher


***
***
# Integración ISO9001
---
## Introducción

En base a la misión de calidad de la organización este proyecto promueve la apropiación de las tecnologías de información y los cambios sociales para su aprovechamiento que contribuyen a la gestión universitaria.

Entre las tecnologías y metodologías empleadas por la oficina de desarrollo podemos diferenciar dos procesos:

* El proceso de realizar un nuevo desarrollo
* El mantenimiento de los sistemas en producción
---
## Nuevos desarrollos

* Todo nuevo desarrollo debe asegurar el cumplimiento de los siguientes puntos:
  * Respetar estándares de codificación
  * Implementar tests de unidad y funcionales durante el desarrollo
  * Implementar tests de aceptación
  * Utilizar integración contínua
  * Aplicar revisión de código escalonada a través de Merge Requests provistos
    por la herramienta de versionado
  * Analizar la cobertura de código en los tests
---
## Sistemas en producción

Todo sistema en producción, dependiendo de su criticidad, debe considerar
* Automatización de su instalación
* Política de backups simplificada por el punto anterior, debido a que únicamente debe backupearse el dato que genere una aplicación y no la aplicación completa
* Política de actualizaciones de seguridad
* Monitoreo y alerta anticipada de fallo en los sistemas:
  * A través de notificaciones ante excepciones fatales en los sistemas
  * A través de la monitorización inteligente de los servicios
---
## Monitorización inteligente

* Se entiende por monitorización inteligente:
  * Llevando estadísticas sobre los sistemas, servicios, y recursos de sistemas
  * Correlacionando lo datos estadísticos recolectados
  * Creando tickets en nuestra mesa de ayuda en forma de alerta baja o moderada
  * Reaccionando adaptando la infraestructura en forma automática cuando sea posible
  * Notificando a través de telegram o email ante alertas urgentes


---
### Conclusión 

_En base a la política de calidad de la organización de alcanzar la satisfacción de los usuarios, es que atacamos cada una de las etapas por las que atraviesan los sistemas desde su creación, hasta su mantenimiento final._
_Es nuestro propósito, el garantizar la alta disponibilidad de los servicios brindados, sin interrupciones o tiempos de respuesta lentos, así como la provisión de actualizaciones imperceptibles por los usuarios de los sistemas._
***
***
# ¿Consultas?
***
